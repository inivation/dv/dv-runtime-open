#!/usr/bin/env sh

# exit when any command fails
set -e

BRANCH="master"

if [ -n "$1" ] ; then
	BRANCH="$1"
fi

mkdir dv-processing_build
cd dv-processing_build

git clone "https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/inivation/dv/internal/dv-processing-internal.git" .
git checkout "$BRANCH"

mkdir build
cd build

cmake -DCMAKE_INSTALL_PREFIX=/usr -DDVP_ENABLE_TESTS=OFF ..
make -j2 -s
make install

cd ../..
rm -Rf dv-processing_build
