# DV SDK support. Not in function scope, provides backwards compatibility.
FIND_PACKAGE(dv 1.6.0 REQUIRED)

# Add local source directory to include path (backwards compatibility).
INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR})

MESSAGE(
	AUTHOR_WARNING
		"Please remove any usage of 'dv-modules.cmake' and its DV_MODULE_SETUP() function, instead FIND_PACKAGE(dv 1.6.0) is now available and preferred, which will import the dv::sdk target."
)

FUNCTION(DV_MODULE_SETUP)
	# Deprecated function.
	MESSAGE(
		AUTHOR_WARNING
			"Please remove any usage of 'dv-modules.cmake' and its DV_MODULE_SETUP() function, instead FIND_PACKAGE(dv 1.6.0) is now available and preferred, which will import the dv::sdk target."
	)
ENDFUNCTION()
