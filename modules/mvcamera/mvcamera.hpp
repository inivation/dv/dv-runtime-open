#include "../../include/dv-sdk/module.hpp"

#include <arv.h>

class MvCamera : public dv::ModuleBase {
public:
	static const char *initDescription();
	static void initOutputs(dv::OutputDefinitionList &out);
	static void initConfigOptions(dv::RuntimeConfig &config);

	MvCamera();
	~MvCamera() override;

	void run() override;
	void configUpdate() override;

private:
	static constexpr size_t STREAM_BUFFER_NUMBER = 4;

	static void addNewBufferToStream(ArvStream *stream, size_t size);

	ArvCamera *camera;
	ArvStream *stream;

	bool isFrameRateAvailable;
	bool isBinningAvailable;
	bool isExposureAvailable;
	bool isAutoExposureAvailable;
	bool isGainAvailable;
	bool isAutoGainAvailable;

	void startAcquisition(bool startThread = false);
	void stopAcquisition();
	void applyConfiguration();
};

registerModuleClass(MvCamera)
