#include "../../include/dv-sdk/cross/asio_tcptlssocket.hpp"

#include "dv_output.hpp"

#include <dv-processing/io/compression/compression_support.hpp>
#include <dv-processing/io/writer.hpp>

class NetTCPServer;

class Connection : public std::enable_shared_from_this<Connection> {
private:
	NetTCPServer *parent;
	WriteOrderedSocket<TCPTLSSocket> socket;
	uint8_t keepAliveReadSpace;

public:
	Connection(asioTCP::socket s, bool tlsEnabled, asioSSL::context *tlsContext, NetTCPServer *server);
	~Connection();

	void close();
	void start(dv::io::Writer *writer, const std::string_view &infoNode);
	void writeMessage(const std::shared_ptr<const dv::io::support::IODataBuffer> &packet);
	void writeIOHeader(const std::shared_ptr<const dv::io::support::IODataBuffer> &ioHeader);

private:
	void keepAliveByReading();
	void handleError(const boost::system::error_code &error, const char *message);
};

class NetTCPServer : public dv::ModuleBase {
private:
	asio::io_service ioService;
	asioTCP::acceptor acceptor;
	asioTCP::socket acceptorNewSocket;
	asioSSL::context tlsContext;
	bool tlsEnabled;

	std::vector<Connection *> clients;
	dv::OutputEncoder output;
	size_t queuedPackets;

	bool portAutoSelect;

	std::string mInfoNode;
	dv::ModuleStatistics *statistics = nullptr;
	std::unique_ptr<dv::io::Writer> mAedat4Writer;

public:
	static void initInputs(dv::InputDefinitionList &in) {
		in.addInput("output0", dv::Types::anyIdentifier, false);
	}

	static const char *initDescription() {
		return ("Send AEDAT 4 data out via TCP to connected clients (server mode).");
	}

	static void initConfigOptions(dv::RuntimeConfig &config) {
		initConfigOptionsOutputCommon(config);

		// Determine default IP address based on ConfigServer IP configuration.
		auto serverNode = dv::Config::GLOBAL.getNode("/system/server/");
		auto ipAddress  = serverNode.getString("ipAddress");

		config.add("ipAddress", dv::ConfigOption::stringOption("IP address to listen on (server mode).", ipAddress));
		config.add(
			"portNumber", dv::ConfigOption::intOption("Port number to listen on (server mode).", 0, 0, UINT16_MAX));
		config.add(
			"maxConnectionsBacklog", dv::ConfigOption::intOption("Maximum number of pending connections.", 5, 1, 32));
		config.add("maxConcurrentConnections",
			dv::ConfigOption::intOption("Maximum number of concurrent active connections (clients).", 10, 1, 128));
		config.add("maxDataBacklog",
			dv::ConfigOption::intOption(
				"Maximum number of data packets in queue waiting to be sent to clients (-1 for infinite).", 5, -1,
				256));

		config.setPriorityOptions({"ipAddress", "portNumber", "compression"});
	}

	NetTCPServer() :
		acceptor(ioService),
		acceptorNewSocket(ioService),
		tlsContext(asioSSL::context::tlsv12_server),
		tlsEnabled(false),
		output(&config, &log),
		queuedPackets(0),
		portAutoSelect(false) {
		// Add compression information at startup, won't change during execution.
		const auto compressionType = config.get<dv::CfgType::STRING>("compression");
		const auto compressionEnum = dv::parseCompressionTypeFromString(compressionType);

		auto stats = std::make_unique<dv::ModuleStatistics>(&output);
		// Intentional pointer leak, so statistics multiplier could be updated
		statistics = stats.get();

		// The compression modes here are split up - LZ4 compression runs in block independent mode for network
		// connections.
		switch (compressionEnum) {
			case dv::CompressionType::LZ4: {
				LZ4F_preferences_t preferences;
				memset(&preferences, 0, sizeof(preferences));
				preferences.frameInfo.blockMode = LZ4F_blockIndependent;
				mAedat4Writer                   = std::make_unique<dv::io::Writer>(
                    std::make_unique<dv::io::compression::Lz4CompressionSupport>(preferences), std::move(stats));
				break;
			}
			case dv::CompressionType::LZ4_HIGH: {
				// High compression mode
				LZ4F_preferences_t preferences;
				memset(&preferences, 0, sizeof(preferences));
				preferences.frameInfo.blockMode = LZ4F_blockIndependent;
				preferences.compressionLevel    = 9;
				mAedat4Writer                   = std::make_unique<dv::io::Writer>(
                    std::make_unique<dv::io::compression::Lz4CompressionSupport>(preferences), std::move(stats));
				break;
			}
			default:
				mAedat4Writer = std::make_unique<dv::io::Writer>(compressionEnum, std::move(stats));
				break;
		}

		// Required input is always present.
		dv::OutputEncoder::makeOutputNode(
			inputs.infoNode("output0"), moduleNode.getRelativeNode("outInfo/0/"), compressionType);

		// --- Generate IO header starts
		dv::io::support::XMLTreeNode root("outInfo");
		dv::xml_compat::transferNode(moduleNode.getRelativeNode("outInfo/"), root);

		dv::io::support::XMLConfigWriter xml(root);
		mInfoNode = xml.getXMLContent();
		// --- Generate IO header ends

		// Configure acceptor.
		auto endpoint = asioTCP::endpoint(asioIP::address::from_string(config.get<dv::CfgType::STRING>("ipAddress")),
			static_cast<uint16_t>(config.get<dv::CfgType::INT>("portNumber")));

		try {
			acceptor.open(endpoint.protocol());
			acceptor.set_option(asioTCP::socket::reuse_address(true));
			acceptor.bind(endpoint);
			acceptor.listen(config.get<dv::CfgType::INT>("maxConnectionsBacklog"));
		}
		catch (const std::exception &) {
			acceptor.close();
			moduleNode.getRelativeNode("outInfo/").removeNode();
			throw;
		}

		// If port was zero, we want to update with the actual port number.
		if (config.get<dv::CfgType::INT>("portNumber") == 0) {
			config.set<dv::CfgType::INT>("portNumber", acceptor.local_endpoint().port());

			portAutoSelect = true;
		}

		acceptStart();

		log.debug.format("Output server ready on {:s}:{:d}.", config.get<dv::CfgType::STRING>("ipAddress"),
			config.get<dv::CfgType::INT>("portNumber"));

		output.updateStartTime();
	}

	~NetTCPServer() override {
		acceptor.close();

		// Post 'close all connections' to end of async queue,
		// so that any other callbacks, such as pending accepts,
		// are executed first, and we really close all sockets.
		ioService.post([this]() {
			// Close all open connections, hard.
			for (auto client : clients) {
				client->close();
			}
		});

		// Wait for all clients to go away.
		while (!clients.empty()) {
			handleIO();
		}

		// Ensure statistics are up-to-date.
		output.updateStatistics(true);

		// Cleanup manually added output info nodes.
		moduleNode.getRelativeNode("outInfo/").removeNode();

		// Reset port to 0 for auto-select.
		if (portAutoSelect) {
			config.set<dv::CfgType::INT>("portNumber", 0);
		}
	}

	void handleIO() {
		ioService.poll();
#if BOOST_ASIO_NET_NEW_INTERFACE == 1
		ioService.restart();
#else
		ioService.reset();
#endif
	}

	void removeClient(Connection *client) {
		dv::vectorRemove(clients, client);
	}

	void handleData() {
		if (clients.empty()) {
			return;
		}

		// Process waiting connections before adding new data.
		handleIO();

		auto input0 = dvModuleInputGet(moduleData, "output0");

		if (input0 != nullptr) {
			// TODO: this only works with default, known types!
			dv::types::TypedObject input(*dv::io::support::defaultTypeResolver(input0->typeId));
			input.obj = input0->obj;

			auto maxDataBacklog = config.get<dv::CfgType::INT>("maxDataBacklog");

			if ((maxDataBacklog == -1) || (queuedPackets <= static_cast<size_t>(maxDataBacklog))) {
				queuedPackets++;
				statistics->setStatsMultiplier(clients.size());

				mAedat4Writer->writePacket(
					&input, 0, [this](const std::shared_ptr<const dv::io::support::IODataBuffer> buffer) {
						auto outBuffer = dv::shared_ptr_wrap_extra_deleter<const dv::io::support::IODataBuffer>(
							buffer, [this](const dv::io::support::IODataBuffer *) {
								queuedPackets--;
							});

						for (const auto client : clients) {
							client->writeMessage(outBuffer);
						}
					});
			}

			// Prevent the typed object destructor freeing the memory
			input.obj = nullptr;

			dvModuleInputDismiss(moduleData, "output0", input0);
		}
	}

	void run() override {
		// Process waiting connections before adding new data.
		handleIO();

		// Push data into the socket
		handleData();

		// Send data out, process writes.
		handleIO();

		// Check if timeout expired.
		if (output.timeElapsed()) {
			log.warning << "TCP network output timeout elapsed, terminating data transmission server." << std::endl;
			config.setBool("running", false);
		}

		// Write out statistics
		output.updateStatistics(false);
	}

private:
	void acceptStart() {
		acceptor.async_accept(acceptorNewSocket, [this](const boost::system::error_code &error) {
			if (error) {
				// Ignore cancel error, normal on shutdown.
				if (error != asio::error::operation_aborted) {
					log.error.format(
						"Failed to accept connection. Error: {:s} ({:d}).", error.message(), error.value());
				}
			}
			else {
				if (clients.size() >= static_cast<size_t>(config.get<dv::CfgType::INT>("maxConcurrentConnections"))) {
					log.warning.format("Maximum number of clients reached, denying {:s}:{:d}.",
						acceptorNewSocket.remote_endpoint().address().to_string(),
						acceptorNewSocket.remote_endpoint().port());
					acceptorNewSocket.close();
				}
				else {
					auto client
						= std::make_shared<Connection>(std::move(acceptorNewSocket), tlsEnabled, &tlsContext, this);

					clients.push_back(client.get());

					client->start(mAedat4Writer.get(), mInfoNode);
				}

				acceptStart();
			}
		});
	}
};

Connection::Connection(asioTCP::socket s, bool tlsEnabled, asioSSL::context *tlsContext, NetTCPServer *server) :
	parent(server),
	socket(std::move(s), tlsEnabled, tlsContext),
	keepAliveReadSpace(0) {
	parent->log.debug.format(
		"New connection from client {:s}:{:d}.", socket.remote_address().to_string(), socket.remote_port());
}

Connection::~Connection() {
	parent->removeClient(this);

	parent->log.debug.format(
		"Closing connection from client {:s}:{:d}.", socket.remote_address().to_string(), socket.remote_port());
}

void Connection::start(dv::io::Writer *writer, const std::string_view &infoNode) {
	auto self(shared_from_this());

	socket.start(
		[this, self, writer, &infoNode](const boost::system::error_code &error) {
			if (error) {
				handleError(error, "Failed startup (TLS handshake)");
			}
			else {
				writer->writeHeader(
					-1, infoNode, [this](const std::shared_ptr<const dv::io::support::IODataBuffer> header) {
						writeIOHeader(header);
					});

				keepAliveByReading();
			}
		},
		asioSSL::stream_base::server);
}

void Connection::writeIOHeader(const std::shared_ptr<const dv::io::support::IODataBuffer> &ioHeader) {
	const auto self(shared_from_this());

	socket.write(asio::buffer(ioHeader->getData(), ioHeader->getDataSize()),
		[this, self, ioHeader = ioHeader](const boost::system::error_code &error, const size_t /*length*/) {
			if (error) {
				handleError(error, "Failed to write IOHeader data");
			}
		});
}

void Connection::close() {
	socket.close();
}

void Connection::writeMessage(const std::shared_ptr<const dv::io::support::IODataBuffer> &packet) {
	auto self(shared_from_this());

	// Write packet header first.
	socket.write(asio::buffer(reinterpret_cast<const std::byte *>(packet->getHeader()), sizeof(dv::PacketHeader)),
		[this, self, packet = packet](const boost::system::error_code &error, const size_t) {
			if (error) {
				handleError(error, "Failed to write message header");
			}
		});

	// Then write packet content.
	socket.write(asio::buffer(packet->getData(), packet->getDataSize()),
		[this, self, packet = packet](const boost::system::error_code &error, const size_t) {
			if (error) {
				handleError(error, "Failed to write message data");
			}
		});
}

void Connection::keepAliveByReading() {
	auto self(shared_from_this());

	socket.read(asio::buffer(&keepAliveReadSpace, sizeof(keepAliveReadSpace)),
		[this, self](const boost::system::error_code &error, size_t /*length*/) {
			if (error) {
				handleError(error, "Read keep-alive failure");
			}
			else {
				handleError(error, "Detected illegal incoming data");
			}
		});
}

void Connection::handleError(const boost::system::error_code &error, const char *message) {
#if defined(OS_MACOS)
	// On MacOS, trying to write to a socket that's being closed can fail
	// with EPROTOTYPE, so we must filter it out here too, details at:
	// https://erickt.github.io/blog/2014/11/19/adventures-in-debugging-a-potential-osx-kernel-bug/
	if ((error == asio::error::eof) || (error == asio::error::broken_pipe) || (error == asio::error::operation_aborted)
		|| (error == boost::system::errc::wrong_protocol_type)) {
#else
	if ((error == asio::error::eof) || (error == asio::error::broken_pipe)
		|| (error == asio::error::operation_aborted)) {
#endif
		// Handle EOF/shutdown separately.
		parent->log.debug.format("Client {:s}:{:d}: connection closed ({:d}).", socket.remote_address().to_string(),
			socket.remote_port(), error.value());
	}
	else {
		parent->log.error.format("Client {:s}:{:d}: {:s}. Error: {:s} ({:d}).", socket.remote_address().to_string(),
			socket.remote_port(), message, error.message(), error.value());
	}
}

registerModuleClass(NetTCPServer)
