#include "../../include/dv-sdk/module.hpp"

#include <boost/filesystem.hpp>
#include <boost/nowide/cstdio.hpp>

class ExportCsv : public dv::ModuleBase {
public:
	static const char *initDescription();

	static void initInputs(dv::InputDefinitionList &in);

	static void initConfigOptions(dv::RuntimeConfig &config);

	ExportCsv();
	~ExportCsv();

	void run() override;

private:
	boost::filesystem::path path;

	std::FILE *file;

	std::string fileContentsToBeWritten;

	const uint32_t sampleLineSize;
};

registerModuleClass(ExportCsv)
