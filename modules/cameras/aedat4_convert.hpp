#ifndef AEDAT4_CONVERT_H
#define AEDAT4_CONVERT_H

#include "../../include/dv-sdk/module.h"

#include <libcaer/events/common.h>

#ifdef __cplusplus
extern "C" {
#endif

	void dvConvertToAedat4(const caerEventPacketHeaderConst oldPacket, const dvModuleData moduleData);

#ifdef __cplusplus
}
#endif

#endif // AEDAT4_CONVERT_H
