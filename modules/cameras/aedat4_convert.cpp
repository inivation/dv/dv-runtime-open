#include "aedat4_convert.hpp"

#include "../../include/dv-sdk/data/event.hpp"
#include "../../include/dv-sdk/data/frame.hpp"
#include "../../include/dv-sdk/data/imu.hpp"
#include "../../include/dv-sdk/data/trigger.hpp"

#include <libcaercpp/events/frame.hpp>
#include <libcaercpp/events/imu6.hpp>
#include <libcaercpp/events/polarity.hpp>
#include <libcaercpp/events/special.hpp>

void dvConvertToAedat4(const caerEventPacketHeaderConst oldPacket, const dvModuleData moduleData) {
	if ((oldPacket == nullptr) || (moduleData == nullptr)) {
		return;
	}

	if (caerEventPacketHeaderGetEventValid(oldPacket) <= 0) {
		// No valid events, nothing to do.
		return;
	}

	// Get real-time timestamp offset for this camera.
	const dvConfigNodeConst sourceInfoNode = dvConfigNodeGetRelativeNode(moduleData->moduleNode, "sourceInfo/");

	const int64_t tsOffset = dvConfigNodeGetLong(sourceInfoNode, "tsOffset");

	switch (caerEventPacketHeaderGetEventType(oldPacket)) {
		case POLARITY_EVENT: {
			const auto newObject      = dvModuleOutputAllocate(moduleData, "events");
			const auto newEventPacket = static_cast<dv::EventPacket *>(newObject->obj);

			const libcaer::events::PolarityEventPacket oldPacketPolarity(
				const_cast<caerEventPacketHeader>(oldPacket), false);

			newEventPacket->elements.reserve(static_cast<size_t>(oldPacketPolarity.getEventValid()));

			for (const auto &evt : oldPacketPolarity) {
				if (!evt.isValid()) {
					continue;
				}

				newEventPacket->elements.emplace_back(
					tsOffset + evt.getTimestamp64(oldPacketPolarity), evt.getX(), evt.getY(), evt.getPolarity());
			}

			if (newEventPacket->elements.size() > 0) {
				dvModuleOutputCommit(moduleData, "events");
			}

			break;
		}

		case FRAME_EVENT: {
			const libcaer::events::FrameEventPacket oldPacketFrame(const_cast<caerEventPacketHeader>(oldPacket), false);

			for (const auto &evt : oldPacketFrame) {
				if (!evt.isValid()) {
					continue;
				}

				const auto newObject = dvModuleOutputAllocate(moduleData, "frames");
				const auto newFrame  = static_cast<dv::Frame *>(newObject->obj);

				newFrame->timestamp = tsOffset + evt.getTSStartOfExposure64(oldPacketFrame);
				newFrame->exposure  = dv::Duration{evt.getExposureLength()};
				newFrame->source    = dv::FrameSource::SENSOR;
				newFrame->positionX = static_cast<int16_t>(evt.getPositionX());
				newFrame->positionY = static_cast<int16_t>(evt.getPositionY());

				// New frame format specification.
				if (evt.getChannelNumber() == libcaer::events::FrameEvent::colorChannels::RGB) {
					// 16 bits, 3 channels, RGB to 8 bits BGR.
					cv::Mat oldFrame{evt.getLengthY(), evt.getLengthX(), CV_16UC3,
						const_cast<uint16_t *>(evt.getPixelArrayUnsafe())};
					cv::Mat newFrameMat{evt.getLengthY(), evt.getLengthX(), CV_8UC3};

					oldFrame.convertTo(newFrameMat, CV_8U, (1.0 / 256.0), 0);
					cv::cvtColor(newFrameMat, newFrameMat, cv::COLOR_RGB2BGR);

					newFrame->image = newFrameMat;
				}
				else if (evt.getChannelNumber() == libcaer::events::FrameEvent::colorChannels::RGBA) {
					// 16 bits, 4 channels, RGBA to 8 bits BGRA.
					cv::Mat oldFrame{evt.getLengthY(), evt.getLengthX(), CV_16UC4,
						const_cast<uint16_t *>(evt.getPixelArrayUnsafe())};
					cv::Mat newFrameMat{evt.getLengthY(), evt.getLengthX(), CV_8UC4};

					oldFrame.convertTo(newFrameMat, CV_8U, (1.0 / 256.0), 0);
					cv::cvtColor(newFrameMat, newFrameMat, cv::COLOR_RGBA2BGRA);

					newFrame->image = newFrameMat;
				}
				else {
					// Default: 16 bits, 1 channel, grayscale to 8 bits grayscale.
					cv::Mat oldFrame{evt.getLengthY(), evt.getLengthX(), CV_16UC1,
						const_cast<uint16_t *>(evt.getPixelArrayUnsafe())};
					cv::Mat newFrameMat{evt.getLengthY(), evt.getLengthX(), CV_8UC1};

					oldFrame.convertTo(newFrameMat, CV_8U, (1.0 / 256.0), 0);
					// No color format conversion needed here.

					newFrame->image = newFrameMat;
				}

				if (!newFrame->image.empty()) {
					dvModuleOutputCommit(moduleData, "frames");
				}
			}

			break;
		}

		case IMU6_EVENT: {
			const auto newObject    = dvModuleOutputAllocate(moduleData, "imu");
			const auto newIMUPacket = static_cast<dv::IMUPacket *>(newObject->obj);

			const libcaer::events::IMU6EventPacket oldPacketIMU(const_cast<caerEventPacketHeader>(oldPacket), false);

			newIMUPacket->elements.reserve(static_cast<size_t>(oldPacketIMU.getEventValid()));

			for (const auto &evt : oldPacketIMU) {
				if (!evt.isValid()) {
					continue;
				}

				dv::IMU imu{};
				imu.timestamp      = tsOffset + evt.getTimestamp64(oldPacketIMU);
				imu.temperature    = evt.getTemp();
				imu.accelerometerX = evt.getAccelX();
				imu.accelerometerY = evt.getAccelY();
				imu.accelerometerZ = evt.getAccelZ();
				imu.gyroscopeX     = evt.getGyroX();
				imu.gyroscopeY     = evt.getGyroY();
				imu.gyroscopeZ     = evt.getGyroZ();

				newIMUPacket->elements.push_back(imu);
			}

			if (newIMUPacket->elements.size() > 0) {
				dvModuleOutputCommit(moduleData, "imu");
			}

			break;
		}

		case SPECIAL_EVENT: {
			const auto newObject        = dvModuleOutputAllocate(moduleData, "triggers");
			const auto newTriggerPacket = static_cast<dv::TriggerPacket *>(newObject->obj);

			const libcaer::events::SpecialEventPacket oldPacketSpecial(
				const_cast<caerEventPacketHeader>(oldPacket), false);

			newTriggerPacket->elements.reserve(static_cast<size_t>(oldPacketSpecial.getEventValid()));

			for (const auto &evt : oldPacketSpecial) {
				if (!evt.isValid()) {
					continue;
				}

				dv::Trigger trigger{};

				if (evt.getType() == TIMESTAMP_RESET) {
					trigger.type = dv::TriggerType::TIMESTAMP_RESET;
				}
				else if (evt.getType() == EXTERNAL_INPUT_RISING_EDGE) {
					trigger.type = dv::TriggerType::EXTERNAL_SIGNAL_RISING_EDGE;
				}
				else if (evt.getType() == EXTERNAL_INPUT_FALLING_EDGE) {
					trigger.type = dv::TriggerType::EXTERNAL_SIGNAL_FALLING_EDGE;
				}
				else if (evt.getType() == EXTERNAL_INPUT_PULSE) {
					trigger.type = dv::TriggerType::EXTERNAL_SIGNAL_PULSE;
				}
				else if (evt.getType() == EXTERNAL_GENERATOR_RISING_EDGE) {
					trigger.type = dv::TriggerType::EXTERNAL_GENERATOR_RISING_EDGE;
				}
				else if (evt.getType() == EXTERNAL_GENERATOR_FALLING_EDGE) {
					trigger.type = dv::TriggerType::EXTERNAL_GENERATOR_FALLING_EDGE;
				}
				else if (evt.getType() == APS_FRAME_START) {
					trigger.type = dv::TriggerType::APS_FRAME_START;
				}
				else if (evt.getType() == APS_FRAME_END) {
					trigger.type = dv::TriggerType::APS_FRAME_END;
				}
				else if (evt.getType() == APS_EXPOSURE_START) {
					trigger.type = dv::TriggerType::APS_EXPOSURE_START;
				}
				else if (evt.getType() == APS_EXPOSURE_END) {
					trigger.type = dv::TriggerType::APS_EXPOSURE_END;
				}
				else {
					continue;
				}

				trigger.timestamp = tsOffset + evt.getTimestamp64(oldPacketSpecial);

				newTriggerPacket->elements.push_back(trigger);
			}

			if (newTriggerPacket->elements.size() > 0) {
				dvModuleOutputCommit(moduleData, "triggers");
			}

			break;
		}

		default:
			// Unknown data.
			break;
	}
}
