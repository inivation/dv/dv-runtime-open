#include "../../include/dv-sdk/data/event.hpp"
#include "../../include/dv-sdk/data/trigger.hpp"
#include "../../include/dv-sdk/module.hpp"

#include "../../src/log.hpp"
#include "aedat4_convert.hpp"

#include <libcaercpp/devices/device_discover.hpp>
#include <libcaercpp/devices/edvs.hpp>

#include <chrono>

class edvs : public dv::ModuleBase {
private:
	libcaer::devices::edvs device;

public:
	static void initOutputs(dv::OutputDefinitionList &out) {
		out.addEventOutput("events");
		out.addTriggerOutput("triggers");
	}

	static const char *initDescription() {
		return ("iniVation eDVS-4337 and mini-eDVS camera support.");
	}

	static void initConfigOptions(dv::RuntimeConfig &config) {
		// Serial port settings.
#if defined(OS_WINDOWS)
		config.add("serialPort", dv::ConfigOption::stringOption("Serial port to connect to.", "COM0", 1, 32));
#else
		config.add("serialPort", dv::ConfigOption::stringOption("Serial port to connect to.", "/dev/ttyUSB0", 1, 32));
#endif
		config.add("baudRate", dv::ConfigOption::intOption(
								   "Baud-rate for serial port.", CAER_HOST_CONFIG_SERIAL_BAUD_RATE_12M, 1, 20000000));

		// Custom camera ID for unique originDescription.
		config.add(
			"cameraID", dv::ConfigOption::stringOption(
							"Custom string to generate an unique origin description, to identify data streams better "
							"for recording or calibration. Use a serial number printed on the device or similar.",
							"", 0, 64));

		biasConfigCreate(config);
		dvsConfigCreate(config);
		serialConfigCreate(config);
		systemConfigCreate(config);

		config.setPriorityOptions({"cameraID"});
	}

	edvs() : device(0, config.getString("serialPort"), static_cast<uint32_t>(config.getInt("baudRate"))) {
		// Initialize per-device log-level to module log-level.
		config.setString("logLevel", "WARNING");
		device.configSet(CAER_HOST_CONFIG_LOG, CAER_HOST_CONFIG_LOG_LEVEL,
			static_cast<uint32_t>(dv::LoggerInternal::logLevelNameToInteger(config.getString("logLevel"))));

		auto devInfo = device.infoGet();

		// Generate source string for output modules.
		auto sourceString = std::string("eDVS4337");

		// No serial number available via software. User can specify one.
		if (!config.getString("cameraID").empty()) {
			sourceString += "_" + config.getString("cameraID");
		}

		// Setup outputs.
		outputs.getEventOutput("events").setup(device.infoGet().dvsSizeX, device.infoGet().dvsSizeY, sourceString);
		outputs.getTriggerOutput("triggers").setup(sourceString);

		auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");

		sourceInfoNode.create<dv::CfgType::STRING>("serialPort", devInfo.serialPortName, {0, 128},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device serial port.");
		sourceInfoNode.create<dv::CfgType::INT>("baudRate", static_cast<int32_t>(devInfo.serialBaudRate), {0, 20000000},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device serial port baud-rate.");

		sourceInfoNode.create<dv::CfgType::BOOL>("deviceIsMaster", devInfo.deviceIsMaster, {},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Timestamp synchronization support: device master status.");

		sourceInfoNode.create<dv::CfgType::STRING>("source", sourceString,
			{static_cast<int32_t>(sourceString.length()), static_cast<int32_t>(sourceString.length())},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device source information.");

		// Ensure good defaults for data acquisition settings.
		// No blocking behavior due to mainloop notification, and no auto-start of
		// all producers to ensure cAER settings are respected.
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_BLOCKING, true);
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_START_PRODUCERS, false);
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_STOP_PRODUCERS, true);

		// Create default settings and send them to the device.
		biasConfigSend();
		systemConfigSend();
		serialConfigSend();
		dvsConfigSend();

		// Set timestamp offset for real-time timestamps. DataStart() will
		// reset the device-side timestamp.
		int64_t tsNowOffset
			= std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch())
				  .count();

		sourceInfoNode.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Time offset of data stream starting point to Unix time in µs.");

		moduleNode.getRelativeNode("outputs/events/info/")
			.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
				"Time offset of data stream starting point to Unix time in µs.");

		moduleNode.getRelativeNode("outputs/triggers/info/")
			.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
				"Time offset of data stream starting point to Unix time in µs.");

		// Start data acquisition.
		device.dataStart(nullptr, nullptr, nullptr, &moduleShutdownNotify, moduleData->moduleNode);

		// Add config listeners last, to avoid having them dangling if Init doesn't succeed.
		moduleNode.getRelativeNode("bias/").addAttributeListener(&device, &biasConfigListener);

		moduleNode.getRelativeNode("dvs/").addAttributeListener(&device, &dvsConfigListener);

		moduleNode.getRelativeNode("serial/").addAttributeListener(&device, &serialConfigListener);

		moduleNode.getRelativeNode("system/").addAttributeListener(&device, &systemConfigListener);

		moduleNode.addAttributeListener(&device, &logLevelListener);
	}

	~edvs() override {
		// Remove listener, which can reference invalid memory in userData.
		moduleNode.getRelativeNode("bias/").removeAttributeListener(&device, &biasConfigListener);

		moduleNode.getRelativeNode("dvs/").removeAttributeListener(&device, &dvsConfigListener);

		moduleNode.getRelativeNode("serial/").removeAttributeListener(&device, &serialConfigListener);

		moduleNode.getRelativeNode("system/").removeAttributeListener(&device, &systemConfigListener);

		moduleNode.removeAttributeListener(&device, &logLevelListener);

		// Stop data acquisition.
		device.dataStop();

		// Clear sourceInfo node.
		auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");
		sourceInfoNode.removeAllAttributes();
	}

	void run() override {
		auto data = device.dataGet();

		if (!data || data->empty()) {
			return;
		}

		if (data->getEventPacket(SPECIAL_EVENT)) {
			std::shared_ptr<const libcaer::events::SpecialEventPacket> special
				= std::static_pointer_cast<libcaer::events::SpecialEventPacket>(data->getEventPacket(SPECIAL_EVENT));

			if (special->getEventNumber() == 1 && (*special)[0].getType() == TIMESTAMP_RESET) {
				// Update master/slave information.
				auto devInfo = device.infoGet();

				auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");
				sourceInfoNode.updateReadOnly<dv::CfgType::BOOL>("deviceIsMaster", devInfo.deviceIsMaster);

				// Reset real-time timestamp offset.
				int64_t tsNowOffset = std::chrono::duration_cast<std::chrono::microseconds>(
					std::chrono::system_clock::now().time_since_epoch())
										  .count();

				sourceInfoNode.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);

				moduleNode.getRelativeNode("outputs/events/info/")
					.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);

				moduleNode.getRelativeNode("outputs/triggers/info/")
					.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);
			}

			dvConvertToAedat4(special->getHeaderPointer(), moduleData);
		}

		if (data->size() == 1) {
			return;
		}

		if (data->getEventPacket(POLARITY_EVENT)) {
			dvConvertToAedat4(data->getEventPacket(POLARITY_EVENT)->getHeaderPointer(), moduleData);
		}
	}

private:
	static void moduleShutdownNotify(void *p) {
		dv::Cfg::Node moduleNode = static_cast<dvConfigNode>(p);

		// Ensure parent also shuts down (on disconnected device for example).
		moduleNode.putBool("running", false);
	}

	static void biasConfigCreate(dv::RuntimeConfig &config) {
		// Bias settings.
		config.add("bias/cas", dv::ConfigOption::intOption("Photoreceptor cascode.", 1992, 0, (0x01 << 24) - 1));
		config.add(
			"bias/injGnd", dv::ConfigOption::intOption("Differentiator switch level.", 1108364, 0, (0x01 << 24) - 1));
		config.add("bias/reqPd", dv::ConfigOption::intOption("AER request pull-down.", 16777215, 0, (0x01 << 24) - 1));
		config.add(
			"bias/puX", dv::ConfigOption::intOption("2nd dimension AER static pull-up.", 8159221, 0, (0x01 << 24) - 1));
		config.add("bias/diffOff",
			dv::ConfigOption::intOption("OFF threshold - lower to raise threshold.", 132, 0, (0x01 << 24) - 1));
		config.add("bias/req", dv::ConfigOption::intOption("OFF request inverter bias.", 309590, 0, (0x01 << 24) - 1));
		config.add("bias/refr", dv::ConfigOption::intOption("Refractory period.", 969, 0, (0x01 << 24) - 1));
		config.add("bias/puY",
			dv::ConfigOption::intOption("1st dimension AER static pull-up.", 16777215, 0, (0x01 << 24) - 1));
		config.add("bias/diffOn",
			dv::ConfigOption::intOption("ON threshold - higher to raise threshold.", 209996, 0, (0x01 << 24) - 1));
		config.add("bias/diff", dv::ConfigOption::intOption("Differentiator.", 13125, 0, (0x01 << 24) - 1));
		config.add(
			"bias/foll", dv::ConfigOption::intOption("Source follower buffer between photoreceptor and differentiator.",
							 271, 0, (0x01 << 24) - 1));
		config.add("bias/pr", dv::ConfigOption::intOption("Photoreceptor.", 217, 0, (0x01 << 24) - 1));

		config.setPriorityOptions({"bias/diffOn", "bias/diffOff"});
	}

	void biasConfigSend() {
		device.configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_CAS, static_cast<uint32_t>(config.getInt("bias/cas")));
		device.configSet(
			EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_INJGND, static_cast<uint32_t>(config.getInt("bias/injGnd")));
		device.configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_REQPD, static_cast<uint32_t>(config.getInt("bias/reqPd")));
		device.configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_PUX, static_cast<uint32_t>(config.getInt("bias/puX")));
		device.configSet(
			EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_DIFFOFF, static_cast<uint32_t>(config.getInt("bias/diffOff")));
		device.configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_REQ, static_cast<uint32_t>(config.getInt("bias/req")));
		device.configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_REFR, static_cast<uint32_t>(config.getInt("bias/refr")));
		device.configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_PUY, static_cast<uint32_t>(config.getInt("bias/puY")));
		device.configSet(
			EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_DIFFON, static_cast<uint32_t>(config.getInt("bias/diffOn")));
		device.configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_DIFF, static_cast<uint32_t>(config.getInt("bias/diff")));
		device.configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_FOLL, static_cast<uint32_t>(config.getInt("bias/foll")));
		device.configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_PR, static_cast<uint32_t>(config.getInt("bias/pr")));
	}

	static void biasConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::edvs *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_INT && key == "cas") {
				device->configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_CAS, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "injGnd") {
				device->configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_INJGND, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "reqPd") {
				device->configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_REQPD, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "puX") {
				device->configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_PUX, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "diffOff") {
				device->configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_DIFFOFF, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "req") {
				device->configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_REQ, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "refr") {
				device->configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_REFR, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "puY") {
				device->configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_PUY, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "diffOn") {
				device->configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_DIFFON, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "diff") {
				device->configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_DIFF, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "foll") {
				device->configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_FOLL, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "pr") {
				device->configSet(EDVS_CONFIG_BIAS, EDVS_CONFIG_BIAS_PR, static_cast<uint32_t>(changeValue.iint));
			}
		}
	}

	static void dvsConfigCreate(dv::RuntimeConfig &config) {
		// DVS settings.
		config.add("dvs/Run", dv::ConfigOption::boolOption("Run DVS to get polarity events.", true));
		config.add(
			"dvs/TimestampReset", dv::ConfigOption::buttonOption("Reset timestamps to zero.", "Reset timestamps"));

		config.setPriorityOptions({"dvs/"});
	}

	void dvsConfigSend() {
		device.configSet(EDVS_CONFIG_DVS, EDVS_CONFIG_DVS_TIMESTAMP_RESET, config.getBool("dvs/TimestampReset"));
		config.setBool("dvs/TimestampReset", false); // Ensure default value is reset.
		device.configSet(EDVS_CONFIG_DVS, EDVS_CONFIG_DVS_RUN, config.getBool("dvs/Run"));
	}

	static void dvsConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		auto device = static_cast<libcaer::devices::edvs *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_BOOL && key == "TimestampReset" && changeValue.boolean) {
				device->configSet(EDVS_CONFIG_DVS, EDVS_CONFIG_DVS_TIMESTAMP_RESET, changeValue.boolean);

				dvConfigNodeAttributeBooleanReset(node, changeKey);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "Run") {
				device->configSet(EDVS_CONFIG_DVS, EDVS_CONFIG_DVS_RUN, changeValue.boolean);
			}
		}
	}

	static void serialConfigCreate(dv::RuntimeConfig &config) {
		// Serial communication buffer settings.
		config.add(
			"serial/ReadSize", dv::ConfigOption::intOption(
								   "Size in bytes of data buffer for serial port read operations.", 1024, 128, 32768));

		config.setPriorityOptions({"serial/"});
	}

	void serialConfigSend() {
		device.configSet(CAER_HOST_CONFIG_SERIAL, CAER_HOST_CONFIG_SERIAL_READ_SIZE,
			static_cast<uint32_t>(config.getInt("serial/ReadSize")));
	}

	static void serialConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::edvs *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_INT && key == "ReadSize") {
				device->configSet(CAER_HOST_CONFIG_SERIAL, CAER_HOST_CONFIG_SERIAL_READ_SIZE,
					static_cast<uint32_t>(changeValue.iint));
			}
		}
	}

	static void systemConfigCreate(dv::RuntimeConfig &config) {
		// Packet settings (size (in events) and time interval (in µs)).
		config.add("system/PacketContainerMaxPacketSize",
			dv::ConfigOption::intOption("Maximum packet size in events, when any packet reaches this size, the "
										"EventPacketContainer is sent for processing.",
				0, 0, 10 * 1024 * 1024));
		config.add("system/PacketContainerInterval",
			dv::ConfigOption::intOption("Time interval in µs, each sent EventPacketContainer will span this interval.",
				10000, 1, 120 * 1000 * 1000));

		// Ring-buffer setting (only changes value on module init/shutdown cycles).
		config.add("system/DataExchangeBufferSize",
			dv::ConfigOption::intOption(
				"Size of EventPacketContainer queue, used for transfers between data acquisition thread and mainloop.",
				64, 8, 1024));

		config.setPriorityOptions({"system/"});
	}

	void systemConfigSend() {
		device.configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_PACKET_SIZE,
			static_cast<uint32_t>(config.getInt("system/PacketContainerMaxPacketSize")));
		device.configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_INTERVAL,
			static_cast<uint32_t>(config.getInt("system/PacketContainerInterval")));

		// Changes only take effect on module start!
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_BUFFER_SIZE,
			static_cast<uint32_t>(config.getInt("system/DataExchangeBufferSize")));
	}

	static void systemConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::edvs *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_INT && key == "PacketContainerMaxPacketSize") {
				device->configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_PACKET_SIZE,
					static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "PacketContainerInterval") {
				device->configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_INTERVAL,
					static_cast<uint32_t>(changeValue.iint));
			}
		}
	}

	static void logLevelListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::edvs *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED && changeType == DVCFG_TYPE_STRING && key == "logLevel") {
			device->configSet(CAER_HOST_CONFIG_LOG, CAER_HOST_CONFIG_LOG_LEVEL,
				static_cast<uint32_t>(dv::LoggerInternal::logLevelNameToInteger(changeValue.string)));
		}
	}
};

registerModuleClass(edvs)
