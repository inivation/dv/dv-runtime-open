#include "../../include/dv-sdk/module.hpp"

#include <random>

class Decimation : public dv::ModuleBase {
private:
	std::minstd_rand minimalRandom;

public:
	static const char *initDescription() {
		return "Reduce the quantity of events to a given random percentage of the input.";
	}

	static void initInputs(dv::InputDefinitionList &in) {
		in.addEventInput("events");
	}

	static void initOutputs(dv::OutputDefinitionList &out) {
		out.addEventOutput("events");
	}

	static void initConfigOptions(dv::RuntimeConfig &config) {
		config.add("percentage",
			dv::ConfigOption::intOption("Percentage of total events to pass from 0% to 100%.", 100, 0, 100));

		config.setPriorityOptions({"percentage"});
	}

	Decimation() {
		outputs.getEventOutput("events").setup(inputs.getEventInput("events"));
	}

	void run() override {
		auto inEvent  = inputs.getEventInput("events").events();
		auto outEvent = outputs.getEventOutput("events").events();

		// multiplication of the percentage for the max number generated divided per 100
		int32_t percentageAdapted = config.getInt("percentage") * (INT32_MAX / 100);

		for (const auto &event : inEvent) {
			int32_t rndNumber = static_cast<int32_t>(minimalRandom());

			// if the generated number is lower than the percentage adapted to the max generated number, then send event
			if (rndNumber < percentageAdapted) {
				outEvent << event;
			}
		}

		outEvent << dv::commit;
	}
};

registerModuleClass(Decimation)
