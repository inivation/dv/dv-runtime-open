# DV Modules

```{toctree}
---
hidden:
---
/sdk/dv-modules/create-modules/index
/sdk/dv-modules/create-custom-datatype
```

A DV module is a piece of code that applies some sort of operation to data, usually having multiple data inputs and
outputs.

![Module appearance in DV](/assets/dv-screenshots/module.png)

Some modules only have outputs, such as the camera or file playback modules, as they only produce new data for the
framework to consume. Some modules only have inputs, such as the network or file recorder modules, as they send data
outside the framework. When you want to build an application for DV, you most likely want to build a module that takes
on some camera data, and outputs some measurements or visualization outputs.

Every module is essentially a CMake project, that most modern IDEs understand.

In this documentation we show you how to:

- [Create a DV Module](/sdk/dv-modules/create-modules/index.md)
- [Create a Custom Datatype](/sdk/dv-modules/create-custom-datatype.md) to use as input/output for modules.

## Module Files Location

Modules are shared libraries. They are files with the extension *.so* (Linux), *.dylib* (macOS), or *.dll* (Windows).
Modules library files are in:

- `/usr/share/dv/modules` (Linux)
- `/usr/local/share/dv/modules` or `/opt/homebrew/share/dv/modules` (macOS)
- `C:\Program Files\DV\runtime\dv_modules` (Windows)

For a library to be discoverable by DV, it has to be in that directory.
