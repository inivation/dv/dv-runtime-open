# Installation

In order to develop modules for [DV](https://docs.inivation.com/master/software/getting-started/installation.html), the
`dv-sdk` library is required. For this purpose, a working C++ 20 toolchain must be installed on the system. This library
is currently only available on [{fa}`apple` Mac](#macos) and [{fa}`linux` Linux](#linux) systems. This means using one
of the following:

- Apple clang version 14 or newer (on Mac)
- gcc version 10.0 or newer
- clang version 13 or newer

## {fa}`apple` MacOS

### Prerequisites

The recommended toolchain is the standard toolchain provided by Apple with XCode. To install this, run:

```bash
xcode-select --install
```

We also require *cmake* to build applications. The easiest way to install *cmake* is via [Homebrew](https://brew.sh).
Run:

```bash
brew install cmake
```

### Library Installation

The `dv-sdk` library is included in the [installation of dv-runtime](/runtime/installation.md#macos).

If you have a mac with Apple Silicon processor, the development runtime will run natively on it.

## {fa}`linux` Linux

Make sure you have a C++ build environment, including *cmake*, installed on your system.

### Ubuntu Linux

We provide a [PPA repository](https://launchpad.net/~inivation-ppa/+archive/ubuntu/inivation) for Focal (20.04 LTS) and
Jammy (22.04 LTS) on the x86_64, arm64 and armhf architectures.

```bash
sudo add-apt-repository ppa:inivation-ppa/inivation
sudo apt-get update
sudo apt-get install dv-runtime-dev
```

### Fedora Linux

We provide a [COPR repository](https://copr.fedorainfracloud.org/coprs/inivation/inivation/) for Fedora 34, 35, 36 and
rawhide on the x86_64, arm64 and armhf architectures.

```bash
sudo dnf copr enable inivation/inivation
sudo dnf install dv-runtime-devel
```

### Arch Linux

The required files to use `dv-sdk` are provided via the [dv-runtime installation](/runtime/installation.md#arch-linux).

### Gentoo Linux

The required files to use `dv-sdk` are provided via the
[dv-runtime installation](/runtime/installation.md#gentoo-linux).
