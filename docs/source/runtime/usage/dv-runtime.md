# dv-runtime

`dv-runtime` is an executable that launches one instance of our runtime system.

Executable options:

| Option                | Argument                                           | Purpose                                             |
| --------------------- | -------------------------------------------------- | --------------------------------------------------- |
| `-h` or `--help`      | None                                               | Print help text (this table)                        |
| `-v` or `--version`   | None                                               | Print runtime version                               |
| `-i` or `--ipaddress` | IP address <br>(default 127.0.0.1)                 | Use this IP address for config-server to listen on  |
| `-p` or `--port`      | Port number <br>(default 4040)                     | Use this port number for config-server to listen on |
| `-l` or `--log`       | Path to log file <br>(default `~/.dv-runtime.log`) | Use this file to log messages from the runtime      |
| `--systemservice`     | `0` or `1` <br>(default 0)                         | Enable system service mode                          |
| `--console`           | `0` or `1` <br>(default 1)                         | Log messages to the console                         |
| `-c` or `--config`    | Path to xml file                                   | Use this XML as initial configuration               |

**Note1**: Multiple instances of the runtime can be started on the same machine, but they need to be on different
IP/Port combinations.

**Note2**: By default, `dv-runtime` will only access [modules](/sdk/dv-modules/index.md) that are located in the
[default folders](/sdk/dv-modules/index.md#module-files-location). To access modules located in different paths, one can
use the following environment variable:

```
DV_MODULES_PATH=/path/to/folder/containing/your-module.so
```
