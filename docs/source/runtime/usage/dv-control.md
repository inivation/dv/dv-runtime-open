# dv-control

`dv-control` is an executable that starts a Command Line Interface used to control and modify live running instances of
the runtime.

Executable options:

| Option                | Argument                                                       | Purpose                                                                                          |
| --------------------- | -------------------------------------------------------------- | ------------------------------------------------------------------------------------------------ |
| `-h` or `--help`      | None                                                           | Print help text (this table)                                                                     |
| `-i` or `--ipaddress` | IP address <br>(default 127.0.0.1)                             | Use this IP address to connect to a runtime on this address                                      |
| `-p` or `--port`      | Port number <br>(default 4040)                                 | Use this port number to connect to a runtime on this port                                        |
| `--tls`               | Path to a file in the PEM format <br>(default "")              | Enable TLS for connection using the specified CA file (no argument: default CA for verification) |
| `--tlscert`           | Path to a file in the PEM format <br>(default "")              | Uses the specified TLS key file for client authentication                                        |
| `-s` or `--script`    | CLI command <br>(more details [here](#command-line-interface)) | Runs the provided command as if typed in the CLI, then exits                                     |

## Command Line Interface

Once started, `dv-control` becomes a Command Line Interface.

![dv-control CLI](/assets/terminal-screenshots/dv-control.png)

The different available commands and their usages are the following:

| Command                  | Arguments                      | Action                                                                               |
| ------------------------ | ------------------------------ | ------------------------------------------------------------------------------------ |
| `node_exists`            | `node`                         | Returns `true` if the provided `node` exists, else `false`.                          |
| `attr_exists`            | `node` <br> `key`              | Returns `true` if, at the provided `node`, the provided `key` exists, else `false`.  |
| `get_children`           | `node`                         | Returns the names of all children from the provided `node`.                          |
| `get_type`               | `node` <br> `key`              | Returns the type of the provided `key` from the provided `node`.                     |
| `help`/`get_description` | `node` <br> `key`              | Returns the description of the provided `key` from the provided `node`.              |
| `get`                    | `node` <br> `key`              | Returns the value of the provided `key` from the provided `node`.                    |
| `put`/`set`              | `node` <br> `key` <br> `value` | Sets the `value` of the provided `key` from the provided `node`.                     |
| `add_module`             | `name` <br> `library`          | Adds a module from the provided `library` with the provided `name` to the structure. |
| `remove_module`          | `name`                         | Removes the module with the provided `name` from the structure.                      |
| `dump_tree`              | _None_                         | Send a full dump of the current configuration tree.                                  |
| `get_client_id`          | _None_                         | Get the ID of this client.                                                           |
| `quit`/`exit`            | _None_                         | Exits the CLI.                                                                       |

**Note1**:

- `node` arguments are in the form of paths. Ex: `/mainloop/accumulator`.
- `key` arguments are generally attributes (written in _camelCase_) from nodes. Ex: `decayParam`.
- `library` arguments are a name of a module library. Ex: `dv_accumulator`.

**Note2**: The CLI has autocomplete, which means most of the commands, nodes and keys can be auto-completed using the
`tab` key.

**Note3**: To connect the outputs of a module to the inputs of another one, one should use the following:

```
set /mainloop/receiver_module_name/inputs/input_name from sender_module_name[output_name]
```
