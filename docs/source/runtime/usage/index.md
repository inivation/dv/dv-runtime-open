# Usage

```{toctree}
---
hidden:
---
/runtime/usage/dv-runtime
/runtime/usage/dv-control
/runtime/usage/config-server-communication
```

Our runtime system can be used in multiple ways:

- Via our DV GUI Software. All explanations are available on our
  [main documentation page](https://docs.inivation.com/master/software/dv/gui/index.html).
- Via terminal. This is the focus of this documentation.
  - [dv-runtime](/runtime/usage/dv-runtime.md) to launch instances of the runtime system
  - [dv-control](/runtime/usage/dv-control.md) to control and modify live the running instances.
- Via other custom applications, using our provided
  [config server communication](/runtime/usage/config-server-communication.md) (ADVANCED).
