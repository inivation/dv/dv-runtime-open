from test_framework._support import *
from test_framework._dv_interface import dv_interface
from test_framework._test_params import *


@standard_params
def test_16_controlAddMultipleModules(self):
    # Check Run
    check_run(self)

    mytest = dv_interface(test_params["name"] + "_log.txt")
    mytest.remove_all_modules()

    mytest.add_module(dv_module("test1", library="dv_davis"))
    mytest.add_module(dv_module("test2", library="dv_accumulator"))
    mytest.add_module(dv_module("test3", library="dv_undistort"))
    mytest.add_module(dv_module("test4", library="dv_dvs128"))
    mytest.add_module(dv_module("test5", library="dv_dvxplorer"))

    closing = mytest.close(runtime_output=None)
    assert_close(self, closing)
