from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_input
@output_hash("b760ea82c07315a63c6835449f3a3855805fd781c5e1a4ba369ca6bda867e839")
def test_05_03_accumulatorStep(self):
    module_parameters = [["decayFunction", "Step"]]
    module = dv_module("dv_accumulator", [["events", "input[events]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
