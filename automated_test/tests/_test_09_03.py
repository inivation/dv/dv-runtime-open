from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash([
    "5c0dd2bdbc7f7bd49d1fc909c307b61411fc944ef3ef2311e8c30296ff12bbc8",
    "58cf22a4019125bd3729cfdee0a1dbe76ef38a26448a9cdadec1da74c0c237b6"
])
def test_09_03_FrameContrastClahe(self):
    module_parameters = [["contrastAlgorithm", "clahe"]]
    module = dv_module("dv_frame_contrast", [["frames", "input[frames]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
