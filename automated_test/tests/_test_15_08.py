from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_performance_input
@calibration_required
@test_performance
def test_15_08_perf_undistort(self):
    calib_file = test_params["calibration_file"]

    # Check Run
    check_run(self)

    # Check calibration file
    self.assertEqual(os.path.exists(calib_file), True, 'Check if calibration file exists:{}'.format(calib_file))

    module_parameters = [["fitMorePixels", "0.2"], ["calibrationFile", calib_file]]
    module = [
        dv_module("dv_undistort", [["events", "input[events]"]], ["undistortedEvents"],
                  config_options=module_parameters)
    ]

    input_params = [["seekStart", "0"], ["seekEnd", 2000000], ["logLevel", "ERROR"]]
    test_modules_with_input(self, module, input_parameters=input_params)
