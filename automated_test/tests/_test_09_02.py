from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash("3a0c3af03a4cbcb8b2e7d0c782f93176ad497ef5b7f0a10366f610b3633b0f69")
def test_09_02_FrameContrastHistogram(self):
    module_parameters = [["contrastAlgorithm", "histogram_equalization"]]
    module = dv_module("dv_frame_contrast", [["frames", "input[frames]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
