from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_input
@output_hash("c85a14714bca24595405757495d90d5058af5580ad4da6509698b4f2ca7077cf")
def test_05_05_accumulatorEventNumber(self):
    module_parameters = [["decayFunction", "NUMBER"]]
    module = dv_module("dv_accumulator", [["events", "input[events]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
