from test_framework._support import *
from test_framework._test_params import *


@davis_input
# Looks like cv::resize(INTER_LINEAR) has different optimizations on MacOS ARM.
# Therefore we need to provide different expected results for each platform.
@output_hash([
    "284b52f27e9f21c50539b39b647d42bdc1ebae6cb160ee20ab8d738762e0cb77",  # All others
    "4d85b0356dcf0f12b54d923781c9db4935581c68f064d8d320e6cd1e31298db0"  # MacOS ARM
])
def test_07_02_CropScaleFrames(self):
    module_parameters = [["cropOffsetX", "50"], ["cropWidth", "100"], ["cropOffsetY", "50"], ["cropHeight", "100"],
                         ["resizeWidth", "200"], ["resizeHeight", "200"], ["resize", "true"]]
    module = dv_module("dv_crop_scale", [["frames", "input[frames]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
