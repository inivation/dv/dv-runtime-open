from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_performance_input
@test_performance
def test_15_03_perf_crop_scale(self):
    module_parameters = [["cropOffsetX", "50"], ["cropWidth", "100"], ["cropOffsetY", "50"], ["cropHeight", "100"],
                         ["resizeWidth", "200"], ["resizeHeight", "200"], ["resize", "true"]]

    module = [dv_module("dv_crop_scale", [["events", "input[events]"]], ["events"], config_options=module_parameters)]

    input_params = [["seekStart", "0"], ["seekEnd", 2000000], ["logLevel", "ERROR"]]
    test_modules_with_input(self, module, input_parameters=input_params)
