from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_performance_input
@test_performance
def test_09_04_FrameContrast_perf(self):
    modules = [
        dv_module("dv_accumulator", [["events", "input[events]"]], ["frames"]),
        dv_module("dv_frame_contrast", [["frames", "dv_accumulator[frames]"]], ["frames"])
    ]

    input_params = [["seekEnd", 2000000]]
    test_modules_with_input(self, modules, input_parameters=input_params)
