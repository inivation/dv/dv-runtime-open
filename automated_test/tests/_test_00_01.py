from test_framework._support import *
import dv.Control as Control
from test_framework._test_params import *


# Convenience Test: stop any previously running runtime.
@standard_params
def test_00_01_stop_runtime(self):
    control = Control()
    try:
        control.put("/system/", "running", "false")
    except:
        return
    print("Runtime closed")
