from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_input
@output_hash("fbabb097f0c4c13f4f5a69530947e3c5da29c20be438bf70dc8529eac185c8d9")
def test_05_02_accumulatorLinear(self):
    module_parameters = [["decayFunction", "Linear"]]
    module = dv_module("dv_accumulator", [["events", "input[events]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
