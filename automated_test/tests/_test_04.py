from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hashes([["output0_hash", "98b66bba82125400b2107778f0339a5b9fcfa646e53c7c9034019b0183a6697f"],
                ["output1_hash", "dcc4628006730d58839032aad0a6af682e92dfea753e88ccc2479223334f82d5"],
                ["output2_hash", "d85f044503e2f1506b8e09984d7c6d0579c39e3f535f2764a86fa25b85b57b31"]])
@output_written_data_sizes([['output0_written_data_size', 621249606], ['output1_written_data_size', 609870],
                            ['output2_written_data_size', 23611430]])
def test_04_InputOutput(self):
    output_inputs = ["events", "imu", "frames"]
    test_io_with_no_module(self, output_inputs)
