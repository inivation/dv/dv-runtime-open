from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash("60e43b9535f6aff4dc09548b84171a1e8be0e7e310f3fb32ccd115f030d58ff3")
def test_09_01_FrameContrastNormalisation(self):
    module = dv_module("dv_frame_contrast", [["frames", "input[frames]"]], ["frames"])
    test_modules_with_io(self, [module])
